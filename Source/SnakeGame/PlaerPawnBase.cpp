// Fill out your copyright notice in the Description page of Project Settings.


#include "PlaerPawnBase.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "SnakeBase.h"
#include "Components/InputComponent.h"

// Sets default values
APlaerPawnBase::APlaerPawnBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));
	RootComponent = PawnCamera;
}

// Called when the game starts or when spawned
void APlaerPawnBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorRotation(FRotator(-90, 0, 0));
	CreateSnakeActor();
}

// Called every frame
void APlaerPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
}

// Called to bind functionality to input
void APlaerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("Verticale", this, &APlaerPawnBase::HandlePlayerVerticaleInput);
	PlayerInputComponent->BindAxis("Horizontale", this, &APlaerPawnBase::HandlePlayerHorizontaleInput);
}

void APlaerPawnBase::CreateSnakeActor()
{
	SnakeActor = GetWorld()->SpawnActor<ASnakeBase>(SnakeActorClass, FTransform());
}

void APlaerPawnBase::HandlePlayerVerticaleInput(float value)
{
	if (IsValid(SnakeActor))
	{
		if (value > 0 && SnakeActor->LastMoveDirection != EMovmentDirection::DOWN)
		{
			SnakeActor->LastMoveDirection = EMovmentDirection::UP;
		}
		else if (value < 0 && SnakeActor->LastMoveDirection != EMovmentDirection::UP)
		{
			SnakeActor->LastMoveDirection = EMovmentDirection::DOWN;
		}
	}
}

void APlaerPawnBase::HandlePlayerHorizontaleInput(float value)
{
	if (IsValid(SnakeActor))
	{
		if (value > 0 && SnakeActor->LastMoveDirection != EMovmentDirection::LEFT)
		{
			SnakeActor->LastMoveDirection = EMovmentDirection::RIGHT;
		}
		else if (value < 0 && SnakeActor->LastMoveDirection !=EMovmentDirection::RIGHT)
		{
			SnakeActor->LastMoveDirection = EMovmentDirection::LEFT;
		}
	}
}

